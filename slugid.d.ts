declare module "slugid";

export function decode(string): string;
export function encode(string): string;
