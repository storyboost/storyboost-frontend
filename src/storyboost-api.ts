import axios from "axios";

export type ProjectEntryResource = {
    id: string;
    name: string;
}

export type ProjectCreateResource = {
    name: string;
}

export type ProjectFileEntryResource = {
    id: string;
    name: string;
}

const API_URL = process.env.NEXT_PUBLIC_STORYBOOST_BACKEND;

export class StoryboostApi {
    static async get_project_entries(): Promise<ProjectEntryResource[]> {
        const response = await axios.get(API_URL + "/api/projects/entries");
        return await response.data;
    }

    static async get_project_entry(id: string): Promise<ProjectEntryResource> {
        const response = await axios.get(API_URL + "/api/projects/entries/" + id);
        return await response.data;
    }

    static async delete_project_entry(id: string) {
        await axios.delete(API_URL + "/api/projects/entries/" + id);
    }

    static async post_project_create(project: ProjectCreateResource) {
        await axios.post(API_URL + "/api/projects/create", project);
    }

    static async post_upload_project_file(id: string, file: File) {
        let data = new FormData();
        data.append("file", file);

        await axios.post(API_URL + "/api/projects/entries/" + id + "/upload-file", data);
    }

    static async get_project_files(id: string): Promise<ProjectFileEntryResource[]> {
        const response = await axios.get(
            API_URL + "/api/project-files/entries",
            {
                params: { projectId: id }
            }
        );
        return await response.data;
    }

    static async get_project_file_download(id: string): Promise<string> {
        const response = await axios.get(
            API_URL + "/api/project-files/entries/" + id + "/download-link"
        );
        return await response.data;
    }
}