import { createMuiTheme } from '@material-ui/core/styles';

const storyboostTheme = createMuiTheme({
    palette: {
        background: {
            default: "#fff",
        }
    }
});

export default storyboostTheme;