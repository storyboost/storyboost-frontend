import React from "react";
import { AppProps } from "next/app"
import Link from "next/link";
import Head from "next/head";
import { ThemeProvider, CssBaseline, AppBar, Typography, Toolbar, Button, makeStyles, createStyles, Theme, Box } from "@material-ui/core";

import theme from "src/theme"

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        titleContainer: {
            flexGrow: 1,
            marginLeft: theme.spacing(2),
        },
        buttonContainer: {
            marginRight: theme.spacing(2),
        },
        title: {
            textDecoration: "none",
        },
    }),
);

export default function MyApp({ Component, pageProps }: AppProps) {
    React.useEffect(() => {
        // Remove the server-side injected CSS.
        const jssStyles = document.querySelector('#jss-server-side');
        if (jssStyles) {
            jssStyles.parentElement!.removeChild(jssStyles);
        }
    }, []);

    const classes = useStyles();

    return (
        <React.Fragment>
            <Head>
                <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
            </Head>
            <ThemeProvider theme={theme}>
                <CssBaseline />

                <AppBar position="relative">
                    <Toolbar variant="dense" disableGutters={true}>
                        <Box className={classes.titleContainer}>
                            <Link href="/" as="/" passHref={true}>
                                <Typography variant="h6" component="a" color="inherit" className={classes.title}>
                                    Storyboost
                                </Typography>
                            </Link>
                        </Box>
                        <Box className={classes.buttonContainer}>
                            <Button color="inherit" disabled={true}>Login</Button>
                        </Box>
                    </Toolbar>
                </AppBar>
                <Component {...pageProps} />
            </ThemeProvider>
        </React.Fragment>
    )
}
