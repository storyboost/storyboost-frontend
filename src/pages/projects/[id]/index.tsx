import { Component } from "react";
import { GetStaticProps } from "next";
import Head from "next/head";
import { Box, Grid, Typography, Table, TableContainer, TableRow, TableBody, TableHead, TableCell, Paper, Button, TextField, createMuiTheme, ThemeProvider, withStyles, Link } from "@material-ui/core";
import DescriptionIcon from "@material-ui/icons/Description";

import slugid from "slugid";

import { ProjectEntryResource, ProjectFileEntryResource, StoryboostApi } from "src/storyboost-api";
import { UploadFileForm } from "src/components/UploadFileForm";
import { DeleteProjectForm } from "src/components/DeleteProjectForm";

type ProjectPageProps = {
    project: ProjectEntryResource;
    files: ProjectFileEntryResource[];
}

export default class ProjectPage extends Component<ProjectPageProps, void> {
    constructor(props: ProjectPageProps) {
        super(props);
    }

    downloadFile = async (id: string) => {
        const link = await StoryboostApi.get_project_file_download(id);

        // Create a dummy link
        var element = document.createElement('a');
        element.setAttribute("href", link);
        element.setAttribute("download", "");
        element.style.display = 'none';

        // Add, click, and immediately remove the link
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    render() {
        const files = this.props.files.map(file =>
            <TableRow key={file.id}>
                <TableCell>
                    <DescriptionIcon style={{ margin: "-0.4rem 0.5rem -0.4rem 0" }} />
                    <Link href="#" onClick={() => this.downloadFile(file.id)}>{file.name}</Link>
                </TableCell>
            </TableRow>
        );

        return (
            <Box padding={2}>
                <Head><title>Storyboost - Project</title></Head>

                <Grid container direction="column" spacing={4}>
                    <Grid item>
                        <Typography variant="h3" component="h1">
                            Project - {this.props.project.name}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="h4" component="h2">Files</Typography>

                        <TableContainer component={Paper}>
                            <Table size="small">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {files}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>

                    <Grid item>
                        <UploadFileForm projectId={this.props.project.id} />
                    </Grid>

                    <Grid item>
                        <DeleteProjectForm project={this.props.project} />
                    </Grid>
                </Grid>
            </Box>
        )
    }
}

export const getServerSideProps: GetStaticProps<ProjectPageProps> = async context => {
    const id = slugid.decode(context.params!.id as string);
    const project = await StoryboostApi.get_project_entry(id);

    const files = await StoryboostApi.get_project_files(id);

    return {
        props: {
            project: project,
            files: files,
        },
    }
}
