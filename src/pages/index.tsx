import { Component, FormEvent } from "react";
import { GetStaticProps } from "next";
import Link from "next/link";
import Head from "next/head";
import { Box, Grid, Typography, Paper } from "@material-ui/core";
import { Alert } from "@material-ui/lab";

import slugid from "slugid";

import { ProjectEntryResource, StoryboostApi } from "src/storyboost-api";
import { CreateProjectForm } from "src/components/CreateProjectForm";

type HomePageProps = {
    projects: ProjectEntryResource[];
}

type HomePageState = {
    projects: ProjectEntryResource[];
    projectName: string;
}

export default class HomePage extends Component<HomePageProps, HomePageState> {
    constructor(props: HomePageProps) {
        super(props);
    
        this.state = {
            projects: props.projects,
            projectName: "",
        };
    }

    handleSubmitted = async () => {
        const projects = await StoryboostApi.get_project_entries();
        this.setState({ projects: projects });
    }

    render() {
        const projects = this.state.projects.map(project =>
            <div key={project.id}>
                <Link
                    href="/projects/[id]"
                    as={"/projects/" + slugid.encode(project.id)}
                    passHref={true}
                >
                    <a>{project.name}</a>
                </Link>
            </div>
        );

        return (
            <Box padding={2}>
                <Head><title>Storyboost</title></Head>

                <Grid container direction="column" spacing={4}>
                    <Grid item>
                        <Alert severity="warning">
                            Storyboost is under heavy development, and files may sometimes go missing.
                            Always keep a backup of your files!
                        </Alert>
                    </Grid>

                    <Grid item>
                        <Typography variant="h4" component="h2">Projects</Typography>
                        {projects}
                    </Grid>

                    <Grid item>
                        <CreateProjectForm onSubmitted={this.handleSubmitted} />
                    </Grid>
                </Grid>
            </Box>
        )
    }
}

export const getServerSideProps: GetStaticProps = async _context => {
    const projects = await StoryboostApi.get_project_entries();

    return {
        props: {
            projects: projects,
        },
    }
}
