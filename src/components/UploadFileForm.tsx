import { Component, ChangeEvent, createRef, FormEvent } from "react";
import { Button, makeStyles, createStyles, Theme, withStyles, StyledComponentProps, WithStyles, Box, Grid, Typography } from "@material-ui/core";

import { StoryboostApi } from "src/storyboost-api";

const styles = createStyles({
    hiddenFileInput: {
        width: "0.1px",
        height: "0.1px",
        opacity: 0,
        overflow: "hidden",
        position: "absolute",
        zIndex: -1,
    }
});

type UploadFileFormProps = WithStyles<typeof styles> & {
    projectId: string,
}

type UploadFileFormState = {
    selectedFile: File | null;
}

class UploadFileFormInner extends Component<UploadFileFormProps, UploadFileFormState> {
    fileInput = createRef<HTMLInputElement>();

    constructor(props: UploadFileFormProps) {
        super(props);

        this.state = {
            selectedFile: null
        }
    }

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({
            selectedFile: event.currentTarget.files![0]
        });
    }

    handleSubmit = async (event: FormEvent) => {
        event.preventDefault();

        await StoryboostApi.post_upload_project_file(
            this.props.projectId,
            this.state.selectedFile!,
        );

        this.fileInput.current!.value = "";
        this.setState({ selectedFile: null });
    }

    render() {
        const submitDisabled = this.state.selectedFile == null;

        return (
            <Grid container component="form" direction="column" onSubmit={this.handleSubmit} spacing={2}>
                <Grid item>
                    <Typography variant="h4" component="h2">Upload New File</Typography>
                </Grid>
                <Grid item>
                    <Button component="label" variant="contained">
                        {this.state.selectedFile
                            ? "File: " + this.state.selectedFile.name
                            : "Choose a file..."}
                        <input
                            className={this.props.classes!.hiddenFileInput}
                            type="file"
                            id="file"
                            onChange={this.handleChange}
                            ref={this.fileInput}
                        />
                    </Button>
                </Grid>
                <Grid item>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        disabled={submitDisabled}
                    >
                        Upload
                    </Button>
                </Grid>
            </Grid>
        )
    }
}

export const UploadFileForm = withStyles(styles, { name: "UploadFileForm" })(UploadFileFormInner);
