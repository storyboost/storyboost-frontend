import { Component, FormEvent, ChangeEvent } from "react";
import Router from "next/router";
import { Box, Grid, Typography, Paper, Button, TextField, withStyles } from "@material-ui/core";
import { red } from "@material-ui/core/colors";

import { ProjectEntryResource, StoryboostApi } from "src/storyboost-api";

const WhiteTextField = withStyles({
    root: {
        "& label": {
            color: "white",
        },
        "& label.Mui-focused": {
            color: "white",
        },
        "& .MuiInput-input": {
            color: "white",
        },
        "& .MuiInput-underline:before": {
            borderBottomColor: "white",
        },
        "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
            borderBottomColor: "white",
        },
        "& .MuiInput-underline:after": {
            borderBottomColor: "white",
        },
    },
}, { name: "WhiteTextField" })(TextField);

const WhiteButton = withStyles({
    root: {
        "&.MuiButton-outlined": {
            borderColor: "rgba(255, 255, 255, 1.0)",
        },
        "&.Mui-disabled": {
            borderColor: "rgba(255, 255, 255, 0.12)"
        },
        "& .MuiButton-label": {
            color: "rgba(255, 255, 255, 1.0)",
        },
        "&.Mui-disabled .MuiButton-label": {
            color: "rgba(255, 255, 255, 0.26)",
        }
    }
}, { name: "WhiteButton" })(Button);

type ProjectPageProps = {
    project: ProjectEntryResource;
}

type ProjectPageState = {
    deleteProjectName: string;
    deleteDisabled: boolean,
}

export class DeleteProjectForm extends Component<ProjectPageProps, ProjectPageState> {
    constructor(props: ProjectPageProps) {
        super(props);

        this.state = {
            deleteProjectName: "",
            deleteDisabled: true,
        }
    }

    handleSubmit = async (event: FormEvent) => {
        event.preventDefault();

        // Just to make sure
        if (this.state.deleteProjectName != this.props.project.name) {
            return;
        }

        await StoryboostApi.delete_project_entry(this.props.project.id!);

        Router.push("/");
    }

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const disabled = event.target.value != this.props.project.name;

        this.setState({ deleteProjectName: event.target.value, deleteDisabled: disabled });
    }

    render() {
        return (
            <Box style={{ backgroundColor: red[500], color: "#fff" }} padding={2} component={Paper}>
                <Grid container direction="column" spacing={2}>
                    <Grid item>
                        <Typography variant="h4" component="h2">Delete Project</Typography>
                    </Grid>

                    <Grid item>
                        This is permanent and cannot be un-done.
                        To confirm, type the name of the project.
                    </Grid>

                    <Grid item onSubmit={this.handleSubmit}>
                        <Grid container component="form" direction="column" spacing={2}>
                            <Grid item>
                                <WhiteTextField
                                    type="text"
                                    label="Project Name"
                                    onChange={this.handleChange}
                                />
                            </Grid>
                            <Grid item>
                                <WhiteButton
                                    type="submit"
                                    variant="outlined"
                                    disabled={this.state.deleteDisabled}
                                >
                                    Delete
                                </WhiteButton>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        )
    }
}
