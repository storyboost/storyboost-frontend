import { FunctionComponent } from "react";
import { Formik, Form, FormikHelpers, Field, FieldProps, FormikProps } from "formik";

import { StoryboostApi } from "src/storyboost-api";
import { Button, TextField, Grid, Typography } from "@material-ui/core";

type CreateProjectFormProps = {
    onSubmitted?: () => Promise<void>;
};

export const CreateProjectForm: FunctionComponent<CreateProjectFormProps> = (props) => {
    const handleSubmit = async (values: any, helpers: FormikHelpers<{}>) => {
        // Submit the project
        const project = { name: values.name };
        await StoryboostApi.post_project_create(project);

        helpers.setSubmitting(false);
        helpers.resetForm();
        await props.onSubmitted?.();
    }

    const validateName = (value: string) => {
        let error;

        if (!value) {
            error = "Project name is required";
        }

        return error;
    }

    return (
        <Formik<any> initialValues={{ name: "" }} isInitialValid={false} onSubmit={handleSubmit}>
            {({ isSubmitting, isValid }: FormikProps<any>) => (
                <Grid container component={Form} spacing={2} direction="column">
                    <Grid item>
                        <Typography variant="h4" component="h2">Create Project</Typography>
                    </Grid>
                    <Grid item>
                        <Field name="name" validate={validateName}>
                            {({ field, form }: FieldProps) => (
                                <TextField
                                    {...field}
                                    label="Project Name"
                                    helperText={form.errors.name}
                                    error={!!form.errors.name}
                                    fullWidth={true}
                                />
                            )}
                        </Field>
                    </Grid>
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={ isSubmitting || !isValid }
                        >
                            Create
                        </Button>
                    </Grid>
                </Grid>
            )}
        </Formik>
    )
}
