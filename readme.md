# Storyboost Frontend

## Development Environment

Create a `.env.local` file with the minimal configuration for your system.

```
NEXT_PUBLIC_STORYBOOST_BACKEND=http://127.0.0.1:5000
```

Run nextjs in development mode.

```bash
npm run dev
```
